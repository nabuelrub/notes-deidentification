![de-id poster.png](https://bitbucket.org/repo/eo4EMa/images/1178312163-de-id%20poster.png)

De-identification User Guide
===============================

**Authors:** Jay Urbain 
***
**Contact:** http://ctsi.mcw.edu/ [Gitter](https://gitter.im/text-deid/Lobby)
***
**Copyright:** Copyright (c) 2014 CTSI Of Southeast Wisconsin
***
**License:** [MIT](http://opensource.org/licenses/MIT) 
***
** Funding:** This project was supported by the  National Center for Advancing Translational Sciences, National Institutes of Health, through Grant Number 8UL1TR000055. Its contents are solely the responsibility of the authors and do not necessarily represent the official views of the NIH.
 
##Project: DeIdentificationX2

##Description
Automated de-identification of protected health information from electronic health records.

##Input
SQL select query providing record text to de-identify.
Output: a newly created relational database table containing de-identified records.



```
#!java

Processing flow. Relevant classes are show within (): 
1) Read records to de-identify (deidentification.Deidentification)
2) Blacklist and whitelist processing (deidentificationRegex.compositeRegex) 
3) Pre-processing and regular expression processing. MCW implementation 
(deidentification.mcw.DeidentificationRegexMCW) removes invalid character encodings,
places spaces between mixed capitalization terms and mixed alpha numerics; and
de-id's dates, MRN/ids, phone, email, and addresses.
jUnit tests have been written for regression testing of regular expressions 
(deidentification.mcw.DeidentificationRegexMCWTest).
4) Named entity recognition for identification and de-id of person and location entity types. MCW implementation
(deidentification.mcw.NamedEntityRecognitionMCW) uses 3 Stanford NLP named entity models trained 
are different text repositories. Entiites are replaced with [XXXXX]
5) Output original record text, regex record text, fully de-id'd text, and time-based performance
measurements to output table.
```


```
#!java


Main class: deidentification.DeIdentification

Program arguments (all arguments are required):
-dburl    – Database URL
-login    – Database login name
-password – Database password
-dbname   – Database name
-dbdriver – Database driver. Note: only MySQL has been validated. E.g., "com.mysql.jdbc.Driver"
-nthreads – Number of concurrent threads for processing records.
-recordsperthread 
          – Number of records assigned to each thread.
-query    –  Input query of records to de-identify. Select must be of the following form: 
             "select  id, note_id, note_text, date_off, pat_name, pat_fname, pat_mname, pat_lname, pat_address, pat_city, pat_zip, pat_phone1, pat_phone2, pat_email, pat_birth, pat_ssn, pat_mrn, contact_date”  
		     where:
             id – unique identifier
             note_id – record note id. Does not have to be distinct. E.g., in EPIC, notes can be split.
             note_text – text to be de-identified.
             date_off – numeric offset for date de-identification, e.g., -15 t0 +15. Set to zero to not de-id dates.
             pat_name - patient full name, e.g. Urbain, Jay F; Jay Urbain; Jay F Urbain -- if available otherwise use null
             pat_fname - patient first name                     -- if available otherwise use null
             pat_mname - patient middle name                    -- if available otherwise use null
             pat_lname - patient last name                    -- if available otherwise use null
             pat_address - patient address                    -- if available otherwise use null
             pat_city - patient city                      -- if available otherwise use null
             pat_zip - patient zip codes e.g. 2391; 75070-2391                -- if available otherwise use null
             pat_phone1 - patient home phone e.g. 505-555-4444                -- if available otherwise use null
             pat_phone2 - patient work phone e.g. 505-555-4444                -- if available otherwise use null
             pat_email - patient email e.g. Jay@gmail.com               -- if available otherwise use null
             pat_birth - patient birth date 'DD-MM-YYYY'                -- if available otherwise use null
             pat_ssn - patient SSN e.g. 111-11-1111                 -- if available otherwise use null
             pat_mrn - patient MRN e.g. 3333333                   -- if available otherwise use null
             contact_date - patient contact date                  -- if available otherwise use null
-deidnotestablename 
          – name of database output table. The program will automatically create this table.
-whitelistfilename 
          – text file containing terms to NOT de-identify. Phrases are currently tokenized and treated as 
		  individual words. E.g., “Saranofsky,” is interpreted as a name, but it is also a common medical 
		  procedure. Whitelist terms are capitalized and pre- and post-fixed with '_'.
-blacklistfilename 
          - text file containing terms to ALWAYS de-identify. Phrases are currently tokenized and treated 
		  as individual words. Blacklist
		  terms are replaced with [XXXXX] in the output text.
-logfile
          - log file containing status update, keeps track of the number of records processed so far, 
          along with some statistics related to the DEID process.
-namedentityrecognitionclass 
          - Named entity class. Must implement the "deidentification.NamedEntityRecognition" abstract class.
-regexdeidentificationclass 
          - Regular expression class. Must implement the "deidentification.DeidentificationRegex" interface.


```

```
#!java

Program arguments with sample values:

-dburl "jdbc:mysql://proto1.ctsi.mcw.edu:3306"
-login "jurbain"
-password "xxxxx"
-dbname "nlp_jurbain"
-dbdriver "com.mysql.jdbc.Driver"
-nthreads 5
-recordsperthread 100
-query "select ID as id, NOTE_ID as note_id, NOTE_TEXT as note_text, DATE_OFF as date_off, 
PAT_NAME as pat_name, PAT_FNAME as pat_fname, PAT_MNAME as pat_mname, PAT_LNAME as pat_lname, PAT_ADDRESS as pat_address, PAT_CITY as pat_city, PAT_ZIP as pat_zip,
PAT_PHONE1 as pat_phone1, PAT_PHONE2 as pat_phone2, PAT_EMAIL as pat_email, PAT_BIRTH as pat_birth,
PAT_SSN as pat_ssn, PAT_MRN as pat_mrn, CONTACT_DATE as contact_date  
from JAY_HNO_NOTE_TEXT_COMB_RANDOM order by id"
-deidnotestablename "JAY_HNO_NOTE_TEXT_COMB_RANDOM_09122014"
-whitelistfilename "samples/whitelist.txt"
-blacklistfilename "samples/blacklist.txt"
-namedentityrecognitionclass "deidentification.mcw.NamedEntityRecognitionMCW"
-regexdeidentificationclass "deidentification.mcw.DeidentificationRegexMCW"
-logfile "logs.txt"
```


```
#!java

Minimum recommend JVM arguments:
-Xms4096M -Xmx4096M

Sample command line:
java -Xms8192M -Xmx8192M -jar DeIdentificationX2.jar -dburl "jdbc:mysql://system.edu:3306" -login "jurbain" -password "xxxxx" -dbname "nlp_jurbain" -dbdriver "com.mysql.jdbc.Driver" -nthreads 5 -recordsperthread 100 -query "select ID as id, NOTE_ID as note_id, NOTE_TEXT as note_text, DATE_OFF as date_off, 
PAT_NAME as pat_name, PAT_FNAME as pat_fname, PAT_MNAME as pat_mname, PAT_LNAME as pat_lname, PAT_ADDRESS as pat_address, PAT_CITY as pat_city, PAT_ZIP as pat_zip,
PAT_PHONE1 as pat_phone1, PAT_PHONE2 as pat_phone2, PAT_EMAIL as pat_email, PAT_BIRTH as pat_birth,
PAT_SSN as pat_ssn, PAT_MRN as pat_mrn, CONTACT_DATE as contact_date from INPUT_TABLES order by id" -deidnotestablename "OUTPUT_DEIDENTIFIED_TABLE" -whitelistfilename "DeIdentificationX2/samples/whitelist.txt" -blacklistfilename "DeIdentificationX2/samples/blacklist.txt" -namedentityrecognitionclass "deidentification.mcw.NamedEntityRecognitionMCW" -regexdeidentificationclass "deidentification.mcw.DeidentificationRegexMCW"
```


Repository is laid out as an Eclipse project.