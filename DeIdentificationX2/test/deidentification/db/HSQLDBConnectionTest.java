package deidentification.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import junit.framework.TestCase;

/**
 * @author Matt Hoag
 */
public class HSQLDBConnectionTest extends TestCase {

    public static final String HSQL_URL = "jdbc:hsqldb:mem:test";
    static String TABLE_NAME = "TEST_NOTES";
    static String[] COLUMN_NAMES = {"ID", "NOTE_ID", "ORIG_NOTE_TEXT", "REGEX_NOTE_TEXT", "DEID_NOTE_TEXT",
        "MSECS_NER", "MSECS_REGEX"};
    private Connection connection;

    public void setUp() throws Exception {
        Properties dbprops = new Properties();
        this.connection = DriverManager.getConnection(HSQL_URL, dbprops);
    }

    public void tearDown() throws Exception {
        connection.close();
    }

    public void testLocalizationID() throws Exception {
        assertEquals(connection.getMetaData().getDatabaseProductName(), new HSQLDBLocalization().getLocalizationID());
    }

    public void testCreateDeidNotesTable() throws Exception {
        DBLocalization hsqlLocalization = DBLocalization.Registry.getLocalization(connection);

        hsqlLocalization.createDeidNotesTable(connection, TABLE_NAME, false);
        java.sql.Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(
                "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.SYSTEM_COLUMNS WHERE TABLE_NAME LIKE '" + TABLE_NAME + "'");

        HashSet<String> expectedColumns
                = new HashSet<>(Arrays.asList(COLUMN_NAMES));
        HashSet<String> actualColumns = new HashSet<>();
        while (resultSet.next()) {
            actualColumns.add(resultSet.getString(1));
        }

        assertEquals(expectedColumns, actualColumns);
    }
}
